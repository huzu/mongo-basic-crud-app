class Person
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :street, type: String
  field :city, type: String
  field :state, type: String

  embeds_in :addresses
end
